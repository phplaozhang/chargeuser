package com.uu.chargeuser.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import java.util.Date;

@Entity
@Table(name = "member")
@JsonIgnoreProperties({"handler","hibernateLazyInitializer"})
public class Member {
    @Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "sn")
    @NotBlank(message = "账号不能为空")
    private String sn;

    private String password;
    private String platform_sn;
    private boolean type;
    private boolean role;
    private boolean operator_sn;
    private String project_sn;
    private String mobile;
    private double balance;
    private double actual_balance;
    private double discount_balance;
    private String avatar;
    private String wechat_openid;
    private String alipay_userid;
    private boolean subscribe;
    private String nickname;
    private String country;
    private String province;
    private String city;
    private boolean sex;
    private int charge_count;
    private int one_charge_minuts;
    private double cancel_noload_check;
    private Date first_charge_time;
    private String last_login_ip;
    private Date last_login_time;
    private Date create_time;
    private Date update_time;

    @Override
    public String toString() {
        return "Member{" +
                "sn='" + sn + '\'' +
                ", password='" + password + '\'' +
                ", platform_sn='" + platform_sn + '\'' +
                ", type=" + type +
                ", role=" + role +
                ", operator_sn=" + operator_sn +
                ", project_sn='" + project_sn + '\'' +
                ", mobile='" + mobile + '\'' +
                ", balance=" + balance +
                ", actual_balance=" + actual_balance +
                ", discount_balance=" + discount_balance +
                ", avatar='" + avatar + '\'' +
                ", wechat_openid='" + wechat_openid + '\'' +
                ", alipay_userid='" + alipay_userid + '\'' +
                ", subscribe=" + subscribe +
                ", nickname='" + nickname + '\'' +
                ", country='" + country + '\'' +
                ", province='" + province + '\'' +
                ", city='" + city + '\'' +
                ", sex=" + sex +
                ", charge_count=" + charge_count +
                ", one_charge_minuts=" + one_charge_minuts +
                ", cancel_noload_check=" + cancel_noload_check +
                ", first_charge_time=" + first_charge_time +
                ", last_login_ip='" + last_login_ip + '\'' +
                ", last_login_time=" + last_login_time +
                ", create_time=" + create_time +
                ", update_time=" + update_time +
                '}';
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPlatform_sn() {
        return platform_sn;
    }

    public void setPlatform_sn(String platform_sn) {
        this.platform_sn = platform_sn;
    }

    public boolean isType() {
        return type;
    }

    public void setType(boolean type) {
        this.type = type;
    }

    public boolean isRole() {
        return role;
    }

    public void setRole(boolean role) {
        this.role = role;
    }

    public boolean isOperator_sn() {
        return operator_sn;
    }

    public void setOperator_sn(boolean operator_sn) {
        this.operator_sn = operator_sn;
    }

    public String getProject_sn() {
        return project_sn;
    }

    public void setProject_sn(String project_sn) {
        this.project_sn = project_sn;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public double getActual_balance() {
        return actual_balance;
    }

    public void setActual_balance(double actual_balance) {
        this.actual_balance = actual_balance;
    }

    public double getDiscount_balance() {
        return discount_balance;
    }

    public void setDiscount_balance(double discount_balance) {
        this.discount_balance = discount_balance;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getWechat_openid() {
        return wechat_openid;
    }

    public void setWechat_openid(String wechat_openid) {
        this.wechat_openid = wechat_openid;
    }

    public String getAlipay_userid() {
        return alipay_userid;
    }

    public void setAlipay_userid(String alipay_userid) {
        this.alipay_userid = alipay_userid;
    }

    public boolean isSubscribe() {
        return subscribe;
    }

    public void setSubscribe(boolean subscribe) {
        this.subscribe = subscribe;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public boolean isSex() {
        return sex;
    }

    public void setSex(boolean sex) {
        this.sex = sex;
    }

    public int getCharge_count() {
        return charge_count;
    }

    public void setCharge_count(int charge_count) {
        this.charge_count = charge_count;
    }

    public int getOne_charge_minuts() {
        return one_charge_minuts;
    }

    public void setOne_charge_minuts(int one_charge_minuts) {
        this.one_charge_minuts = one_charge_minuts;
    }

    public double getCancel_noload_check() {
        return cancel_noload_check;
    }

    public void setCancel_noload_check(double cancel_noload_check) {
        this.cancel_noload_check = cancel_noload_check;
    }

    public Date getFirst_charge_time() {
        return first_charge_time;
    }

    public void setFirst_charge_time(Date first_charge_time) {
        this.first_charge_time = first_charge_time;
    }

    public String getLast_login_ip() {
        return last_login_ip;
    }

    public void setLast_login_ip(String last_login_ip) {
        this.last_login_ip = last_login_ip;
    }

    public Date getLast_login_time() {
        return last_login_time;
    }

    public void setLast_login_time(Date last_login_time) {
        this.last_login_time = last_login_time;
    }

    public Date getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Date create_time) {
        this.create_time = create_time;
    }

    public Date getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(Date update_time) {
        this.update_time = update_time;
    }
}
