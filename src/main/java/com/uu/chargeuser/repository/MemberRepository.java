package com.uu.chargeuser.repository;

import com.uu.chargeuser.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository<Member,Integer> {
    Member findBySn(String sn);
}