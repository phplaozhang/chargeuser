package com.uu.chargeuser.service;


import com.uu.chargeuser.entity.Member;

public interface MemberService {
    Member getMember(String sn);
    Member saveMember(Member member);
}
