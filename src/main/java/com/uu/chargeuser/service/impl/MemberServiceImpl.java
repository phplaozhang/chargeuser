package com.uu.chargeuser.service.impl;

import com.uu.chargeuser.entity.Member;
import com.uu.chargeuser.repository.MemberRepository;
import com.uu.chargeuser.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MemberServiceImpl implements MemberService {
    @Autowired
    MemberRepository memberRepository;

    @Override
    public Member getMember(String sn) {
        return memberRepository.findBySn(sn);
    }

    @Override
    public Member saveMember(Member member) {
        Member save = memberRepository.save(member);
        return save;
    }
}

