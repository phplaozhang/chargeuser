package com.uu.chargeuser.controller;

import com.uu.chargeuser.entity.Member;
import com.uu.chargeuser.result.Result;
import com.uu.chargeuser.result.ResultFactory;
import com.uu.chargeuser.service.impl.MemberServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.Objects;

@RestController
@RequestMapping("/member")
public class MemberController {
    @Autowired
    MemberServiceImpl memberServiceImpl;
    @GetMapping("/findMember")
    public Result findMemberBySn(String sn){
        return ResultFactory.buildSuccessResult(memberServiceImpl.getMember(sn).toString());
    }
    @PostMapping("/saveMember")
    public Result saveMember(){
        /*JSONObject jsonObject = JSONObject.parseObject(user);
        Member member = JSONObject.toJavaObject(jsonObject,Member.class );
        //如果都要返回 需要下方的3遍历进行拼接返回，或者map集合返回
        if (bindingResult.hasErrors()){
            return ResultFactory.buildFailResult(bindingResult.getFieldError().getDefaultMessage());
        }*/

        for (int i = 1;i < 100000;i ++) {

            Member member = new Member();
            member.setSn(i + "" );
            member.setPassword(i + "密码");
            member.setPlatform_sn(i + "123456");
            member.setType(false);
            member.setRole(false);
            member.setOperator_sn(false);
            member.setProject_sn(i + "123");
            member.setMobile("18733333333");
            member.setBalance(0.03);
            member.setActual_balance(0.03);
            member.setDiscount_balance(0.03);
            member.setAvatar("123");
            member.setWechat_openid("afauhafaa1f5af5a");
            member.setAlipay_userid("ashfafaf231321");
            member.setSubscribe(false);
            member.setNickname("hello");
            member.setCountry("中国");
            member.setProvince("哈哈");
            member.setCity(i + "sss");
            member.setSex(true);
            member.setCharge_count(12);
            member.setOne_charge_minuts(5);
            member.setCancel_noload_check(123);
            member.setFirst_charge_time(new Date());
            member.setLast_login_ip("1.2.3.4.5");
            member.setLast_login_time(new Date());
            member.setCreate_time(new Date());
            member.setUpdate_time(new Date());
            member.setActual_balance(0.00);
            member.setAlipay_userid("123");
            memberServiceImpl.saveMember(member);
        }

        return ResultFactory.buildSuccessResult("新增成功");
    }

}