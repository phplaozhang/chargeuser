package com.uu.chargeuser.controller;


import com.uu.chargeuser.extend.Db;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/demo")

public class DemoController {

    @GetMapping("/test")
    public void test() {


        //查询
        /*

        List where  = new ArrayList();

        where.add(new ArrayList<String>(){{
            add("sn");
            add("=");
            add("200612100011");
        }});

        Db db = new Db();
        List<Map> member1 = db.getConnection().table("member").where(where).order("sn asc").limit(10).page(1).select("*");

        for(Map x : member1) {
            System.out.println(x);
        }*/

        //删除
        /*List where  = new ArrayList();
        where.add(new ArrayList<String>(){{
            add("sn");
            add("=");
            add("200612100005");
        }});
        Db db = new Db();
        int res = db.getConnection().table("member").where(where).delete();
        System.out.println(res);*/

        //修改
        /*List where  = new ArrayList();
        where.add(new ArrayList<String>(){{
            add("sn");
            add("<=");
            add("200612100009");
        }});

        Map upate = new HashMap(){
            {
                put("password", "1");
                put("country", "2");
            }
        };

        Db db = new Db();
        int res = db.getConnection().table("member").where(where).update(upate);
        System.out.println(res);*/

        //插入一条
        /*Map insert = new HashMap(){
            {
                put("password", "100");
                put("sn", "1110");
            }
        };

        Db db = new Db();
        int res = db.getConnection().table("member").insert(insert);
        System.out.println(res);*/

        //插入多条
        /*List insert = new ArrayList();
        insert.add(new HashMap(){
            {
                put("password", "10000");
                put("sn", "10003");
            }
        });
        insert.add(new HashMap(){
            {
                put("password", "20000");
                put("sn", "20003");
            }
        });

        Db db = new Db();
        int res = db.getConnection().table("member").insertAll(insert);
        System.out.println(res);*/


        List where  = new ArrayList();

        where.add(new ArrayList<String>(){{
            add("sn");
            add("=");
            add("1000");
        }});

        Db db = new Db();
        int member1 = db.getConnection().table("member").where(where).count("*");
        System.out.println(member1);


        /*List where  = new ArrayList();

        where.add(new ArrayList<String>(){{
            add("sn");
            add("=");
            add("200612100006");
        }});

        Db db = new Db();
        List<Map> member1 = db.getConnection().table("member").where(where).select("*");

        for(Map x : member1) {
            System.out.println(x);
        }*/


        //打印sql语句
        System.out.println(db.getLastSql());
    }
}
