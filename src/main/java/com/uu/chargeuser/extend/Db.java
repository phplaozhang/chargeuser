package com.uu.chargeuser.extend;


import org.springframework.core.env.Environment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import javax.annotation.PostConstruct;
import java.sql.*; //导包
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Component
public class Db {


    // 数据库连接对象
    private static Connection Conn;

    //数据库驱动类
    private static String Driver;

    // 数据库连接地址
    private static String URL;

    // 数据库的用户名
    private static String UserName;

    // 数据库的密码
    private static String Password;

    //表名称
    private static String Table;

    //sql条件
    private static List<List<String>> Where = new ArrayList<>();

    //sql排序
    private static String Order = " create_time desc ";

    //sql页大小
    private static int Limit = 100;

    //sql页码
    private static int Page = 1;

    //sql语句
    private static String Sql = "";

    //
    private static List<Object> Parameter = new ArrayList<Object>();

    @Autowired
    private Environment env;

    @PostConstruct
    public void config() {
        Driver = env.getProperty("spring.datasource.driver-class-name");
        URL = env.getProperty("spring.datasource.url");
        UserName = env.getProperty("spring.datasource.username");
        Password = env.getProperty("spring.datasource.password");
    }



    public Db getConnection() {
        try {
            Class.forName(Driver); // 加载驱动
            //System.out.println("加载驱动成功!!!");
        } catch (ClassNotFoundException e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        try {
            //通过DriverManager类的getConenction方法指定三个参数,连接数据库
            Conn = DriverManager.getConnection(URL, UserName, Password);
            //System.out.println("连接数据库成功!!!");
            return this;
        } catch (SQLException e) {
            // TODO: handle exception
            e.printStackTrace();
            return null;
        }
    }

    public Db table(String table) {
        Table = " " + table + " ";
        return this;
    }

    public void setParams(Object x){
        Parameter.add(x);
    }

    public Db where(List list){

        if(!list.isEmpty()){
            Where = list;
        }
        return this;
    }

    public Db order(String order){
        if(!order.isEmpty()){
            Order =  order;
        }
        return this;
    }

    public Db limit(int limit){
        if(limit > 0){
            Limit = limit;
        }
        return this;
    }

    public Db page(int page){
        if(page > 0){
            Page = page;
        }
        return this;
    }

    public String getLastSql(){
        return Sql;
    }

    public int count(String fields){
        String fields_sql = fields.isEmpty() ? "*" : "" + fields + "";

        //组装sql语句
        String showSql = "SELECT COUNT(" + fields_sql + ") FROM ";
        String sql = "SELECT COUNT(" + fields_sql + ") FROM ";

        //表名称
        showSql += " " + Table + " ";
        sql += " " + Table + " ";

        //查询条件
        if(!Where.isEmpty()){
            showSql += " WHERE 1 = 1 ";
            sql += " WHERE 1 = 1  ";
            for(List x : Where) {
                showSql += " AND " + x.get(0) + " " + x.get(1) + " " + x.get(2) + " ";
                sql += " AND " + x.get(0) + " " + x.get(1) + " ? ";
                setParams(x.get(2));
            }
        }


        Sql = showSql;



        PreparedStatement pstm = null;
        ResultSet rs = null;
        int count = 0;
        try{
            pstm = Conn.prepareStatement(sql);
            for (int i = 0;i < Parameter.size();i ++){
                pstm.setObject((i + 1), Parameter.get(i));
            }
            rs = pstm.executeQuery();
            if(rs.next()){
                count = (int) rs.getObject(1);
            }
            return count;
        }catch(Exception e){
            e.printStackTrace();
            return count;
        } finally {
            try {
                rs.close();
                pstm.close();
                Conn.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }


    }

    public Map find(String fields) throws SQLException {
        List reList = select(fields);
        Map reMap = (Map) reList.get(0);
        return reMap;

    }



    public List select(String fields) {
        String fields_sql = fields.isEmpty() ? "*" : "" + fields + "";
        int offset = (Page - 1) * Limit;

        //组装sql语句
        String showSql = "SELECT " + fields_sql + " FROM ";
        String sql = "SELECT " + fields_sql + " FROM ";

        //表名称
        showSql += " " + Table + " ";
        sql += " " + Table + " ";

        //查询条件
        if(!Where.isEmpty()){
            showSql += " WHERE 1 = 1 ";
            sql += " WHERE 1 = 1  ";
            for(List x : Where) {
                showSql += " AND " + x.get(0) + " " + x.get(1) + " " + x.get(2) + " ";
                sql += " AND " + x.get(0) + " " + x.get(1) + " ? ";
                setParams(x.get(2));
            }
        }

        //排序
        showSql += " ORDER BY " + Order + " ";
        sql += " ORDER BY " + Order + " ";

        //分页
        showSql += " OFFSET " + offset + " ROWS FETCH NEXT " + Limit + " ROWS ONLY ";
        sql += " OFFSET ? ROWS FETCH NEXT ?  ROWS ONLY ";
        setParams(offset);
        setParams(Limit);

        Sql = showSql;

        PreparedStatement pstm = null;
        ResultSet rs = null;
        try{
            pstm = Conn.prepareStatement(sql);
            for (int i = 0;i < Parameter.size();i ++){
                /*if(i == 1){
                    pstm.setTimestamp((i + 1), (Timestamp)Parameter.get(i));
                }else{
                    pstm.setObject((i + 1), Parameter.get(i));
                }*/
                pstm.setObject((i + 1), Parameter.get(i));
            }
            rs = pstm.executeQuery();
            List reList = convertList(rs);
            return reList;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        } finally {
            try {
                rs.close();
                pstm.close();
                Conn.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }

    private List convertList(ResultSet rs) throws SQLException{
        List list = new ArrayList();
        ResultSetMetaData md = rs.getMetaData();//获取键名
        int columnCount = md.getColumnCount();//获取行的数量
        while (rs.next()) {
            Map rowData = new HashMap();//声明Map
            for (int i = 1; i <= columnCount; i++) {
                rowData.put(md.getColumnName(i), rs.getObject(i));//获取键名及值
            }
            list.add(rowData);
        }
        return list;
    }

    public int update(Map<Object, Object> setMap){

        //组装sql语句
        String showSql = "UPDATE " + Table + " SET ";
        String sql = "UPDATE " + Table + " SET ";

        //set
        if(setMap.isEmpty()){
           return 0;
        }
        for(Map.Entry<Object, Object> entry : setMap.entrySet()){
            Object mapKey = entry.getKey();
            Object mapValue = entry.getValue();
            showSql += " " + mapKey + " = " + mapValue + " , ";
            sql += " " + mapKey + " = ? , ";
            setParams(mapValue);
        }
        showSql = showSql.substring(0, showSql.length() - 2) + " ";
        sql = sql.substring(0, sql.length() - 2) + " ";


        //条件
        if(!Where.isEmpty()){
            showSql += " WHERE 1 = 1 ";
            sql += " WHERE 1 = 1  ";
            for(List x : Where) {
                showSql += " AND " + x.get(0) + " " + x.get(1) + " " + x.get(2) + " ";
                sql += " AND " + x.get(0) + " " + x.get(1) + " ? ";
                setParams(x.get(2));
            }
        }

        Sql = showSql;

        PreparedStatement pstm = null;
        int rs = 0;
        try{
            pstm = Conn.prepareStatement(sql);
            for (int i = 0;i < Parameter.size();i ++){
                pstm.setObject((i + 1), Parameter.get(i));
            }
            rs = pstm.executeUpdate();
            return rs;
        }catch(Exception e){
            e.printStackTrace();
            return rs;
        } finally {
            try {
                pstm.close();
                Conn.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }

        }

    }

    public int insert (Map<Object, Object> setMap){

        //INSERT INTO table_name (列1, 列2,...) VALUES (值1, 值2,....)

        //set
        if(setMap.isEmpty()){
            return 0;
        }
        String showSql_fields = " ( ";
        String showSql_values = " ( ";
        String sql_fields = " ( ";
        String sql_values = " ( ";
        for(Map.Entry<Object, Object> entry : setMap.entrySet()){
            Object mapKey = entry.getKey();
            Object mapValue = entry.getValue();
            showSql_fields += " " + mapKey + " , ";
            showSql_values += " " + mapValue + " , ";
            sql_fields += " " + mapKey + " , ";
            sql_values += " ? , ";
            setParams(mapValue);
        }

        showSql_fields = showSql_fields.substring(0, showSql_fields.length() - 2) + " ";
        showSql_values = showSql_values.substring(0, showSql_values.length() - 2) + " ";
        sql_fields = sql_fields.substring(0, sql_fields.length() - 2) + " ";
        sql_values = sql_values.substring(0, sql_values.length() - 2) + " ";

        showSql_fields += " ) ";
        showSql_values += " ) ";
        sql_fields += " ) ";
        sql_values += " ) ";


        //组装sql语句
        String showSql = "INSERT INTO " + Table + " " + showSql_fields + " VALUES " + showSql_values;
        String sql = "INSERT INTO " + Table + " " + sql_fields + " VALUES " + sql_values;

        Sql = showSql;

        PreparedStatement pstm = null;
        int rs = 0;
        try{
            pstm = Conn.prepareStatement(sql);
            for (int i = 0;i < Parameter.size();i ++){
                pstm.setObject((i + 1), Parameter.get(i));
            }
            rs = pstm.executeUpdate();
            return rs;
        }catch(Exception e){
            e.printStackTrace();
            return rs;
        } finally {
            try {
                pstm.close();
                Conn.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }

        }

    }

    public int insertAll(List<Map> insertList){

        //INSERT INTO table_name (列1, 列2,...) VALUES (值1, 值2,....),(值1, 值2,....),(值1, 值2,....)

        //set
        if(insertList.isEmpty()){
            return 0;
        }

        String showSql_fields = " ( ";
        String showSql_values = "";
        String sql_fields = " ( ";
        String sql_values = "";
        for (int i = 0; i < insertList.size(); i ++){
            Map<Object, Object> setMap = insertList.get(i);

            showSql_values += " ( ";
            sql_values += " ( ";
            for(Map.Entry<Object, Object> entry : setMap.entrySet()){
                Object mapKey = entry.getKey();
                Object mapValue = entry.getValue();

                if(i == 0){
                    showSql_fields += " " + mapKey + " , ";
                    sql_fields += " " + mapKey + " , ";
                }

                showSql_values += " " + mapValue + " , ";
                sql_values += " ? , ";
                setParams(mapValue);
            }

            showSql_values = showSql_values.substring(0, showSql_values.length() - 2) + " ";
            sql_values = sql_values.substring(0, sql_values.length() - 2) + " ";

            showSql_values += " ) , ";
            sql_values += " ) , ";
        }

        showSql_fields = showSql_fields.substring(0, showSql_fields.length() - 2) + " ";
        showSql_values = showSql_values.substring(0, showSql_values.length() - 2) + " ";
        sql_fields = sql_fields.substring(0, sql_fields.length() - 2) + " ";
        sql_values = sql_values.substring(0, sql_values.length() - 2) + " ";

        showSql_fields += " ) ";
        sql_fields += " ) ";


        //组装sql语句
        String showSql = "INSERT INTO " + Table + " " + showSql_fields + " VALUES " + showSql_values;
        String sql = "INSERT INTO " + Table + " " + sql_fields + " VALUES " + sql_values;


        Sql = showSql;

        PreparedStatement pstm = null;
        int rs = 0;
        try{
            pstm = Conn.prepareStatement(sql);
            for (int i = 0;i < Parameter.size();i ++){
                pstm.setObject((i + 1), Parameter.get(i));
            }
            rs = pstm.executeUpdate();
            return rs;
        }catch(Exception e){
            e.printStackTrace();
            return rs;
        } finally {
            try {
                pstm.close();
                Conn.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }

        }
    }

    public int delete(){
        // delete from testss.dbo.test1 where id='12';

        //组装sql语句
        String showSql = "DELETE FROM " + Table + " ";
        String sql = "DELETE FROM " + Table + " ";

        //条件
        if(!Where.isEmpty()){
            showSql += " WHERE 1 = 1 ";
            sql += " WHERE 1 = 1  ";
            for(List x : Where) {
                showSql += " AND " + x.get(0) + " " + x.get(1) + " " + x.get(2) + " ";
                sql += " AND " + x.get(0) + " " + x.get(1) + " ? ";
                setParams(x.get(2));
            }
        }

        Sql = showSql;


        PreparedStatement pstm = null;
        int rs = 0;
        try{
            pstm = Conn.prepareStatement(sql);
            for (int i = 0;i < Parameter.size();i ++){
                pstm.setObject((i + 1), Parameter.get(i));
            }
            rs = pstm.executeUpdate();
            return rs;
        }catch(Exception e){
            e.printStackTrace();
            return rs;
        } finally {
            try {
                pstm.close();
                Conn.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }

        }

    }


}